package br.edu.up.jogodavelhamultiplayer;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class MainActivity extends AppCompatActivity {

    private static final int PORTA = 8888;
    public static String TAG = "JogoMultiplayer";

    private boolean isAtivo = true;
    private boolean isRemoto = false;
    private ImageView imgLinha;
    private EditText txtRemoto;
    private ArrayList<View> listaDeViews;
    private char jogadorDaVez = 'o';
    private char[] jogadas = new char[9];
    private int[][] sequencias = {
            {1, 2, 3}, {4, 5, 6}, {7, 8, 9},
            {1, 4, 7}, {2, 5, 8}, {3, 6, 9},
            {1, 5, 9}, {3, 5, 7}
    };

    private Map<Integer, Integer> linhas = new HashMap<>();
    {
        limparJogadas();
    }

    {
        linhas.put(0, R.drawable.linha_horizontal_1);
        linhas.put(1, R.drawable.linha_horizontal_2);
        linhas.put(2, R.drawable.linha_horizontal_3);
        linhas.put(3, R.drawable.linha_vertical_1);
        linhas.put(4, R.drawable.linha_vertical_2);
        linhas.put(5, R.drawable.linha_vertical_3);
        linhas.put(6, R.drawable.linha_decrescente);
        linhas.put(7, R.drawable.linha_crescente);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        listaDeViews = new ArrayList<>();
        LinearLayout linha1 = (LinearLayout) findViewById(R.id.linha1);
        LinearLayout linha2 = (LinearLayout) findViewById(R.id.linha2);
        LinearLayout linha3 = (LinearLayout) findViewById(R.id.linha3);
        listaDeViews.addAll(linha1.getTouchables());
        listaDeViews.addAll(linha2.getTouchables());
        listaDeViews.addAll(linha3.getTouchables());
        imgLinha = (ImageView) findViewById(R.id.imgLinha);

        txtRemoto = (EditText) findViewById(R.id.txtRemoto);

    }

    public void onClickReiniciar(View v) {
        limparJogadas();
    }

    public void limparJogadas() {

        for (int i = 0; i < jogadas.length; i++) {
            jogadas[i] = '-';
        }

        if (imgLinha != null && txtRemoto != null) {

            imgLinha.setImageResource(R.drawable.fundo_transparente);
            for (View view : listaDeViews) {
                ImageView imageView = (ImageView) view;
                imageView.setImageResource(R.drawable.btn_branco);
                imageView.setEnabled(true);

            }

            if (!isRemoto) {
                String servidor = txtRemoto.getText().toString();
                MensageiroTask task = new MensageiroTask();
                task.execute("LIMPAR", servidor, String.valueOf(PORTA));
            }
        }
        isRemoto = false;
    }

    public void onClickJogar(View v) {

        ImageView imageView = (ImageView) v;
        imageView.setEnabled(false);
        String tag = (String) imageView.getTag();
        int posicao = Integer.parseInt(tag);
        jogadas[posicao - 1] = jogadorDaVez;

        for (int i = 0; i < sequencias.length; i++) {

            int[] sequencia = sequencias[i];
            if (jogadas[sequencia[0] - 1] == jogadorDaVez &&
                    jogadas[sequencia[1] - 1] == jogadorDaVez &&
                    jogadas[sequencia[2] - 1] == jogadorDaVez) {

                for (View view : listaDeViews) {
                    ImageView imgv = (ImageView) view;
                    imgv.setEnabled(false);
                }

                imgLinha.setImageResource(linhas.get(i));
            }
        }

        if (!isRemoto) {
            String servidor = txtRemoto.getText().toString();
            MensageiroTask task = new MensageiroTask();
            task.execute(jogadorDaVez + tag, servidor, String.valueOf(PORTA));
        }
        isRemoto = false;

        if (jogadorDaVez == 'o') {
            imageView.setImageResource(R.drawable.o_verde_hdpi);
            jogadorDaVez = 'x';
        } else {
            imageView.setImageResource(R.drawable.x_preto_hdpi);
            jogadorDaVez = 'o';
        }
    }

    private class MensageiroTask extends AsyncTask<String, Void, Void> {
        @Override
        protected Void doInBackground(String... params) {

            String mensagemEnviar = params[0];
            String servidor = params[1];
            int porta = Integer.parseInt(params[2]);

            Socket socket = null;
            DataOutputStream dos = null;

            try {

                socket = new Socket(servidor, porta);
                dos = new DataOutputStream(socket.getOutputStream());
                dos.writeUTF(mensagemEnviar);

            } catch (IOException e) {
                e.printStackTrace();
            } finally {

                if (dos != null) {
                    try {
                        dos.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }

                if (socket != null) {
                    try {
                        socket.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
            return null;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        isAtivo = true;
        new Thread(new BackgroundThread()).start();
    }

    @Override
    protected void onPause() {
        super.onPause();
        isAtivo = false;
    }

    public void processarJogada(String tag){

        int posicao = Integer.parseInt(tag);
        ImageView imageView = (ImageView) listaDeViews.get(posicao -1);
        onClickJogar(imageView);
    }

    private class BackgroundThread implements Runnable {
        @Override
        public void run() {

            ServerSocket serverSocket = null;
            try {
                serverSocket = new ServerSocket(PORTA);

                while (isAtivo) {

                    Socket socket = null;
                    DataInputStream dis = null;

                    try {
                        socket = serverSocket.accept();
                        dis = new DataInputStream(socket.getInputStream());

                        try {
                            final String msg = dis.readUTF();
                            if (msg != null && !msg.equals("")) {
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        isRemoto = true;
                                        Log.d("Palyer", msg);
                                        if (msg.equals("LIMPAR")){
                                            limparJogadas();
                                        } else {
                                            jogadorDaVez = msg.charAt(0);
                                            processarJogada(String.valueOf(msg.charAt(1)));
                                        }
                                    }
                                });
                            }
                        } catch (IOException e) {
                            //Igonorado;
                        }

                    } catch (IOException e) {
                        e.printStackTrace();
                    } finally {

                        if (dis != null) {
                            try {
                                dis.close();
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }

                        if (socket != null) {
                            try {
                                socket.close();
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
            } finally {

                if (serverSocket != null) {
                    try {
                        serverSocket.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }
}